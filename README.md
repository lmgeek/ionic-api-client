# ionic-api-client


### Pre-requisitos 📋
```
git clone https://gitlab.com/lmgeek/ionic-api-client/
```

### Instalar el proyecto 📦
_npm leerá el archivo package.json para ver que dependencias tiene el proyecto y las descargará e instalará en la carpeta node-modules._
```
npm install 
```

### Levantamos el servidor 🚀
_Con esto logramos transpilar el código TypeScript y correrá la aplicación por primera vez en nuestro navegador, además generará las carpetas www y build que son necesarias en el proyecto de Ionic._
```
ionic serve
```

### Reestablecer el etado del proyecto 🛠  ️🔧
_Si pregunta por si se le permite sobrescribir algunos archivos le damos que si. Con este comando se restaurarán las plataformas que tenga agregadas la app (Android o IOS) y los plugins de Ionic y Cordova que están el el archivo config.xml. Algunos plugins necesitan ciertos parámetros para la instalación, estos también están guardados en el archivo config.xml, y al restaurar no vamos a tener ningun problema, tan solo debemos tenerlo en cuenta y fijarnos al momento de incorporar un nuevo plugin al proyecto._
```
ionic cordova prepare
```

### Ejecutar servidor con lab
_Con esto podremos levantar el servidor para ver en diferentes dispositivos a la vez._
```
ionic lab
```


---
⌨️ con ❤️ por [Luis Marin](https://gitlab.com/lmgeek) 😊